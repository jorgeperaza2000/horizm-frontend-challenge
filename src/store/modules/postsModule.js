import api from '../../api/posts.js'

const state = {
  posts: [],
  postsSearched: [],
  post: {},
  editingPost: '',
  options: {
    networks: [],
    types: []
  },
  searching: false
}

const mutations = {
  SET_POSTS (state, posts) {
    state.posts = posts
  },
  SET_EDITING_POST (state, index) {
    state.editingPost = index
  },
  UPDATE_POST (state) {
    Object.assign(state.posts[state.editingPost], state.post)
    // state.posts[state.editingPost] = state.post
    // console.log('PA', state.posts[state.editingPost])
  },
  SET_POST (state, values) {
    state.post = Object.assign({}, state.post, values)
  },
  SET_OPTIONS_DATA (state) {
    const allNetworks = state.posts.map((post) => { return post.network })
    state.options.networks = allNetworks.filter((item, index, network) => {
      return network.indexOf(item) === index
    })
    const allTypes = state.posts.map((post) => { return post.type })
    state.options.types = allTypes.filter((item, index, type) => {
      return type.indexOf(item) === index
    })
  },
  SORT_POST (state, options) {
    let postToOrder = getters.getPosts(state)
    postToOrder = postToOrder.sort((a, b) => {
      let sa = 0
      let sb = 0
      if (options.field === 'date') {
        sa = a.date
        sb = b.date
      } else if (options.field === 'copy') {
        sa = a.copy.toLowerCase()
        sb = b.copy.toLowerCase()
      } else if (options.field === 'type') {
        sa = a.type.toLowerCase()
        sb = b.type.toLowerCase()
      } else if (options.field === 'impressions') {
        sa = parseInt(a.impressions)
        sb = parseInt(b.impressions)
      }
      if (options.order === 'asc') {
        if (sa < sb) { return -1 }
        if (sa > sb) { return 1 }
        return 0
      } else {
        if (sa > sb) { return -1 }
        if (sa < b) { return 1 }
        return 0
      }
    })
    if (state.searching) {
      state.postsSearched = postToOrder
    } else {
      state.posts = postToOrder
    }
  },
  FILTER_POSTS (state, filters) {
    state.searching = false
    state.postsSearched = state.posts
    if (filters._network !== '0') {
      state.searching = true
      state.postsSearched = state.postsSearched.filter((post) => {
        return post.network === filters._network
      })
    }
    if (filters._type !== '0') {
      state.searching = true
      state.postsSearched = state.postsSearched.filter((post) => {
        return post.type === filters._type
      })
    }
    if (filters._message !== '') {
      state.searching = true
      state.postsSearched = state.postsSearched.filter((post) => {
        return post.copy.toLowerCase().includes(filters._message.toLowerCase())
      })
    }
  }
}

const actions = {
  setPosts ({ commit }) {
    return new Promise(resolve => {
      api.getPosts(posts => {
        commit('SET_POSTS', posts)
        commit('SET_OPTIONS_DATA', posts)
        resolve()
      })
    })
  },
  updatePost ({ commit }) {
    commit('UPDATE_POST')
  },
  setPost ({ commit }, post) {
    commit('SET_POST', post)
  },
  setEditingPost ({ commit }, index) {
    commit('SET_EDITING_POST', index)
  },
  sortPost ({ commit }, options) {
    commit('SORT_POST', options)
  },
  filterPosts ({ commit }, filters) {
    commit('FILTER_POSTS', filters)
  }
}

const getters = {
  getPosts: (state) => {
    if (state.searching) {
      return state.postsSearched
    } else {
      return state.posts
    }
  },
  getPost: (state) => {
    return state.post
  },
  getRealPosts: (state) => {
    return state.posts
  },
  getOptionsData: (state) => {
    return state.options
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
