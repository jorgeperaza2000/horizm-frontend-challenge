export default {
  async getPosts (cb) {
    const response = await fetch('/posts_data.json')
    const posts = await response.json()
    cb(posts)
  }
}
